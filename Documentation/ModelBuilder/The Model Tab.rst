
.. index:: Model Tab

The Model Tab
=============

The Model Tab contains a hierarchical tree of everything in the scene. For a discrete model, this means means surfaces, faces, edges, and vertices. Clicking the |pqEyeball16| symbol toggles the visibility of the corresponding part.

.. |pqEyeball16| image:: images/pqEyeball16.png

.. findfigure:: ModelTab.*

Right clicking on any part brings up a context menu with manipulation options.

.. todo::
	Explain visibility, color and the right-click context menu.

