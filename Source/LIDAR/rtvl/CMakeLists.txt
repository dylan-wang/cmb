# Copyright 2007-2009 Brad King, Chuck Stewart
# Distributed under the Boost Software License, Version 1.0.
# (See accompanying file rtvl_license_1_0.txt or copy at
#  http://www.boost.org/LICENSE_1_0.txt)

add_library(rtvl STATIC
  rtvl_intpow.hxx
  rtvl_terms.hxx
  rtvl_tokens.hxx
  rtvl_vote.hxx                         rtvl_vote.txx
  rtvl_votee.hxx                        rtvl_votee.txx
  rtvl_votee_d.hxx                      rtvl_votee_d.txx
  rtvl_voter.hxx                        rtvl_voter.txx
  rtvl_refine.hxx                       rtvl_refine.txx
  rtvl_tensor.hxx                       rtvl_tensor.txx
  rtvl_tensor_d.hxx                     rtvl_tensor_d.txx
  rtvl_weight.hxx
  rtvl_weight_original.hxx              rtvl_weight_original.txx
  rtvl_weight_smooth.hxx                rtvl_weight_smooth.txx

  Templates/rtvl_vote+2-.cxx
  Templates/rtvl_votee+2-.cxx
  Templates/rtvl_votee_d+2-.cxx
  Templates/rtvl_voter+2-.cxx
  Templates/rtvl_refine+2-.cxx
  Templates/rtvl_tensor+2-.cxx
  Templates/rtvl_tensor_d+2-.cxx
  Templates/rtvl_weight_original+2-.cxx
  Templates/rtvl_weight_smooth+2-.cxx

  Templates/rtvl_vote+3-.cxx
  Templates/rtvl_votee+3-.cxx
  Templates/rtvl_votee_d+3-.cxx
  Templates/rtvl_voter+3-.cxx
  Templates/rtvl_refine+3-.cxx
  Templates/rtvl_tensor+3-.cxx
  Templates/rtvl_tensor_d+3-.cxx
  Templates/rtvl_weight_original+3-.cxx
  Templates/rtvl_weight_smooth+3-.cxx
  )

#We need to enable position independence on static libraries so we can
#link them to shared libraries.
set_target_properties( rtvl PROPERTIES POSITION_INDEPENDENT_CODE TRUE)

find_library(RGTL_LIBRARY
  NAMES rgtl
  PATHS ${VXL_LIBRARY_DIR}
  PATH_SUFFIXES
    Release Debug)
find_library(VNL_ALGO_LIBRARY
  NAMES vnl_algo
  PATHS ${VXL_LIBRARY_DIR}
  PATH_SUFFIXES
    Release Debug)
find_library(VNL_LIBRARY
  NAMES vnl
  PATHS ${VXL_LIBRARY_DIR}
  PATH_SUFFIXES
    Release Debug)
find_library(V3P_NETLIB_LIBRARY
  NAMES v3p_netlib
  PATHS ${VXL_LIBRARY_DIR}
  PATH_SUFFIXES
    Release Debug)

#target_link_libraries(rtvl rgtl vnl_algo vnl)
target_link_libraries(rtvl ${RGTL_LIBRARY} ${VNL_ALGO_LIBRARY} ${VNL_LIBRARY} ${V3P_NETLIB_LIBRARY})
#${EXECUTABLE_OUTPUT_PATH}/${CMAKE_CFG_INTDIR}